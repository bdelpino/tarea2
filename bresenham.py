import sys
from main import *

#TAMANIO GRILLA
size_grilla = sys.argv[1].rpartition("x")
ancho = int(size_grilla[0])
alto = int(size_grilla[2])

#PIXEL INICIO
pixel_inicio = sys.argv[2].rpartition(",")
x_i = int(pixel_inicio[0])
y_i = int(pixel_inicio[2])

#PIXEL TERMINO
pixel_final = sys.argv[3].rpartition(",")
x_f = int(pixel_final[0])
y_f = int(pixel_final[2])

main(ancho, alto, x_i, y_i, x_f, y_f)
