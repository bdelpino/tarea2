"""
-------------------------------------------------------------------------------
Importación de librerías
-------------------------------------------------------------------------------
"""
from OpenGL.GL import glBegin
from PyOpenGLtoolbox import *  # Se usará esta librería sólo para manejar la ventana de OpenGL, v2.1.0
import sys
import math
import random

""" Parametros que recibe bresenham3d """
#TAMANIO GRILLA
size_grilla = sys.argv[1].rpartition("x")
ancho = int(size_grilla[0])
alto = int(size_grilla[2])

#PIXEL INICIO
pixel_inicio = sys.argv[2].rpartition(",")
x_i = int(pixel_inicio[0])
y_i = int(pixel_inicio[2])

#PIXEL TERMINO
pixel_final = sys.argv[3].rpartition(",")
x_f = int(pixel_final[0])
y_f = int(pixel_final[2])

"""
-------------------------------------------------------------------------------
Definición de funciones
-------------------------------------------------------------------------------
"""

def reshape_window_perspective(w, h, near, far, fov):
    """
    Crea la ventana con una proyección en perspectiva.
    """
    h = max(h, 1)
    glLoadIdentity()

    # Crea el viewport
    glViewport(0, 0, int(w), int(h))
    glMatrixMode(GL_PROJECTION)

    # Proyección en perspectiva
    gluPerspective(fov, float(w) / float(h), near, far)

    # Setea el modo de la cámara
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()


def bresenham(x_i, y_i, x_f, y_f):
    dx = x_f - x_i
    dy = y_f - y_i

    if dx > 0:
        xsign = 1
    else:
        xsign = -1

    if dy > 0:
        ysign = 1
    else:
        ysign = -1

    dx = abs(dx)
    dy = abs(dy)

    if dx > dy:
        xx = xsign
        xy = 0
        yx = 0
        yy = ysign
    else:
        dx = dy
        dy = dx
        xx = 0
        xy = ysign
        yx = xsign
        yy = 0

    p = 2*dy - dx
    y = 0

    puntos = []
    for x in range(dx + 1):

        pix_xyz = []

        pix_x = x_i + x * xx + y * yx
        pix_xyz.append(pix_x)

        pix_y = y_i + x * xy + y * yy
        pix_xyz.append(pix_y)
        puntos.append(pix_xyz)
        pix_z = 0

        pix_xyz.append(pix_z)
        if p >= 0:
            y += 1
            p -= 2*dx
        p += 2*dy
    return puntos

def rgb(r, g, b):
    return [r / 255.0, g / 255.0, b / 255.0]

# def initLight():
#     glLightfv(GL_LIGHT0, GL_POSITION, [100, 0, 100, 1.0])
#     glLightfv(GL_LIGHT0, GL_AMBIENT, [0.2, 0.2, 0.2, 1.0])
#     glLightfv(GL_LIGHT0, GL_SPECULAR, [1.0, 1.0, 1.0, 1.0])
#     glLightfv(GL_LIGHT0, GL_DIFFUSE, [1.0, 1.0, 1.0, 1.0])
#
#     glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 1.0)
#     glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.0)
#     glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.0)
#
#     glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 180.0)
#     glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, [-1.0, 0.0, 0.0])
#     glLightf(GL_LIGHT0, GL_SPOT_EXPONENT, 0.0)
#
#     glEnable(GL_LIGHT0)

"""
-------------------------------------------------------------------------------
Definición de algunas constantes
-------------------------------------------------------------------------------
"""
FOV = 60
FPS = 60
VENTANA_H = 600
VENTANA_W = 800
# rojo = rgb(255, 0, 0)
# verde = rgb(0, 255, 0)
# azul = rgb(0, 0, 255)
# blanco = rgb(255,255,255)
x_rand = 0
y_rand = 255
z_rand = 0

"""
-------------------------------------------------------------------------------
Inicio de OpenGL
-------------------------------------------------------------------------------
"""
init_pygame(VENTANA_W, VENTANA_H, 'Bresenham Algorithm in 3D', centered_window=True)
init_gl(transparency=False, materialcolor=False, normalized=True, perspectivecorr=True, antialiasing=True,
        depth=True, smooth=True, verbose=True, version=True)
reshape_window_perspective(w=VENTANA_W, h=VENTANA_H, near=0.01, far=1000, fov=60)
clock = pygame.time.Clock()

"""
-------------------------------------------------------------------------------
Creación de modelos
-------------------------------------------------------------------------------
"""

axis = create_axes(300)  # Retorna una lista con los ejes, parte de la librería

# Generamos un cubo estandar(2x2x2)
cubo = create_cube()

# Creamos la linea
bresenhamPix = glGenLists(1)  # Inicia una lista
glNewList(bresenhamPix, GL_COMPILE)

ancho_ventana = ancho * 10
alto_ventana = alto * 10
lado_pix = 10

#Fondo de la grilla
glBegin(GL_QUADS)
glColor3fv(rgb(170, 170, 170))
glVertex3fv([0, 0, 0])
glVertex3fv([ancho_ventana, 0, 0])
glVertex3fv([ancho_ventana, alto_ventana, 0])
glVertex3fv([0, alto_ventana, 0])
glEnd()
# Lineas verticales de la grilla
negro = rgb(0, 0, 0)
pos_x = 0
glBegin(GL_LINES)
while pos_x <= ancho_ventana:
    glColor(negro)
    glVertex3fv([pos_x, 0, 0])
    glVertex3fv([pos_x, alto_ventana, 0])
    pos_x += lado_pix
glEnd()
#Lineas Horizontales
pos_y = 0
glBegin(GL_LINES)
while pos_y <= alto_ventana:
    glColor(negro)
    glVertex3fv([0, pos_y, 0])
    glVertex3fv([ancho_ventana, pos_y, 0])
    pos_y += lado_pix
glEnd()
#Dibuja cubos que representan la linea en grilla
puntos = bresenham(x_i, y_i, x_f, y_f)
for i in puntos:
    pix_x = i[0]
    pix_y = i[1]
    glPushMatrix()
    glTranslate(5 + (10 * pix_x), 5 + (10 * pix_y), 0)
    glColor(rgb(0, 112, 130))
    glScale(5, 5, 5)
    glCallList(cubo)
    glPopMatrix()

glEndList()  # Cierra la lista

"""
-------------------------------------------------------------------------------
Creación de la cámara
-------------------------------------------------------------------------------
"""

CAMARA_POS = [200, 200, 200]  # Donde estoy (x,y,z)
CAMARA_CENTRO = [0, 0, 0]  # Dónde apunto (x,y,z)
CAMARA_NORMAL = [0, 0, 1]  # La camara está parada normal al eje z

"""
-------------------------------------------------------------------------------
Inicio de OpenGL
-------------------------------------------------------------------------------
"""

while True:

    # Tick del reloj
    clock.tick(FPS)

    # Elimina el buffer
    clear_buffer()

    # Ubica la cámara
    glLoadIdentity()
    gluLookAt(CAMARA_POS[0], CAMARA_POS[1], CAMARA_POS[2],
              CAMARA_CENTRO[0], CAMARA_CENTRO[1], CAMARA_CENTRO[2],
              CAMARA_NORMAL[0], CAMARA_NORMAL[1], CAMARA_NORMAL[2])

    #Habitlita la iluminacion
    #glEnable(GL_LIGHTING)

    #configura fuentes de luz
    #initLight()


    #FALTA CONFIGURAR LUCES
    glCallList(axis)
    glPushMatrix()
    glScale(1, 1, 1)
    glCallList(bresenhamPix)
    glPopMatrix()

    # Cilindro que representa linea
    color_rand = rgb(x_rand, y_rand, z_rand)
    x_dif = x_f - x_i
    y_dif = y_f - y_i
    distancia = math.sqrt(abs(math.pow(x_dif, 2) + math.pow(y_dif, 2)))
    angulo = math.degrees(math.atan(y_dif / x_dif))
    glTranslate(puntos[0][0] * 10, puntos[0][1] * 10, 100)
    glRotatef(90, 1, 0, 0)
    glRotatef(90 + angulo, 0, 1, 0)
    glColor(color_rand)
    forma = gluNewQuadric()
    # (objeto cuad, r base, r top, altura, n° de lineas verticales, n° lineas horizontales)
    gluCylinder(forma, 5, 5, distancia * 10, 10000, 10000)
    
    pygame.display.flip()

    # Chequea eventos
    for event in pygame.event.get():
        if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
            exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                x_rand = random.randint(1, 255)
                y_rand = random.randint(1, 255)
                z_rand = random.randint(1, 255)

            keys = pygame.key.get_pressed()
            #CAMARA 1
            if keys[K_1]:
                CAMARA_POS = [200, 200, 200]
                CAMARA_CENTRO = [0, 0, 0]
                CAMARA_NORMAL = [0, 0, 1]
            #CAMARA 2
            elif keys[K_2]:
                CAMARA_POS = [ancho_ventana, alto_ventana, 500]
                CAMARA_CENTRO = [ancho_ventana * 0.5, alto_ventana * 0.5, 0]
                CAMARA_NORMAL = [0, 0, 1]
            #CAMARA 3
            if keys[K_3]:
                CAMARA_POS = [200, 0, 200]
                CAMARA_CENTRO = [0, 0, 0]
                CAMARA_NORMAL = [0, 0, 1]
            #CAMARA 4
            elif keys[K_4]:
                CAMARA_POS = [0,0 , 200]
                CAMARA_CENTRO = [ancho_ventana * 0.5, alto_ventana * 0.5, 0]
                CAMARA_NORMAL = [0, 0, 1]

        # Vuelca el contenido
        pygame.display.flip()