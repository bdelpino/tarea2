import pygame
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *

from PyOpenGLtoolbox import  *


def rgb(r,g,b):
    return [r/255.0, g/255.0, b/255.0]

def plot(i,j,):
    rojo = rgb (255,0,0)
    verde = rgb(0,255,0)
    azul = rgb(0,0,255)
    x = 50 * i
    y = 50 * j

    glBegin(GL_QUADS)
    glColor3fv(rojo)
    glVertex2fv([x, y + 1])
    glVertex2fv([x + 49, y + 1])
    glVertex2fv([x + 49, y + 50])
    glVertex2fv([x, y + 50])
    glEnd()


def grilla(ancho, alto):
    ancho_ventana = ancho * 50
    alto_ventana = alto * 50
    lado_pix = 50

    #Fondo de la grilla
    glBegin(GL_QUADS)
    blanco = rgb(255, 255, 255)
    glColor3fv(blanco)
    glVertex2fv([ancho_ventana, alto_ventana])
    glVertex2fv([0, alto_ventana])
    glVertex2fv([0, 0])
    glVertex2fv([ancho_ventana, 0])
    glEnd()

    # Lineas verticales de la grilla
    negro = rgb(0, 0, 0)
    pos_x = 0
    glBegin(GL_LINES)
    while pos_x <= ancho_ventana:
        glColor(negro)
        glVertex2fv([pos_x, 0])
        glVertex2fv([pos_x, alto_ventana])
        pos_x += lado_pix
    glEnd()

    #Lineas Horizontales
    pos_y = 0
    glBegin(GL_LINES)
    while pos_y <= alto_ventana:
        glColor(negro)
        glVertex2fv([0, pos_y])
        glVertex2fv([ancho_ventana, pos_y])
        pos_y += lado_pix
    glEnd()

#Funcion que retorna una lista con los pixeles de la linea
def bresenham(x_i, y_i, x_f, y_f):
    dx = x_f - x_i
    dy = y_f - y_i

    if dx > 0:
        xsign = 1
    else:
        xsign = -1

    if dy > 0:
        ysign = 1
    else:
        ysign = -1
    
    dx = abs(dx)
    dy = abs(dy)

    if dx > dy:
        xx = xsign
        xy = 0
        yx = 0
        yy = ysign
    else:
        dx = dy
        dy = dx
        xx = 0
        xy = ysign
        yx = xsign
        yy = 0
    
    p = 2*dy - dx
    y = 0

    puntos = []
    for x in range(dx + 1):
        pix_xy = []

        pix_x = x_i + x * xx + y * yx
        pix_xy.append(pix_x)

        pix_y = y_i + x * xy + y * yy
        pix_xy.append(pix_y)

        puntos.append(pix_xy)
        if p >= 0:
            y += 1
            p -= 2*dx
        p += 2*dy
    return puntos

def main(ancho, alto, x_i, y_i, x_f, y_f):
    ancho_ventana = ancho * 50
    alto_ventana = alto * 50

    pygame.init()
    display = (ancho_ventana, alto_ventana)
    pygame.display.set_mode(display, DOUBLEBUF | OPENGL)
    pygame.display.set_caption("Algoritmo de Bresenham")

    #inicializa opengl

    glViewport(0, 0, ancho_ventana, alto_ventana) #establece la ventana grafica
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluOrtho2D(0.0, ancho_ventana, 0.0, alto_ventana)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    # definir variables de opengl
    glClearColor(0.0, 0.0, 0.0, 0.0)  # color del fondo
    glShadeModel(GL_SMOOTH)
    glClearDepth(1.0)

    puntos = bresenham(x_i, y_i, x_f, y_f)
    inicial = 0
    grilla(ancho, alto)
    pygame.display.flip()
    run = True

    print("\nRepresentacion Gráfica del Algoritmo de Bresenham.")
    print("- Para avanzar un paso del algoritmo presiona la tecla hacia la derecha.")
    print("- Para retroceder un paso del algoritmo presiona la tecla hacia la izquierda.\n")

    print("Punto inicial: (" + str(x_i) + ", " + str(y_i) + ")")
    print("Punto final: (" + str(x_f) + ", " + str(y_f) + ")\n")
    print("PRESIONA LA TECLA HACIA LA DERECHA (->) PARA EMPEZAR.\n")

    while run:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                print("Fin del Programa")
                run = False
            
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    inicial -= 1
                    ptos = puntos[:inicial]
                    grilla(ancho, alto)

                    if inicial <= 0:
                        print("\n**No hagas mas pasos a la izquierda, SOLO INTENTA AVANZAR.**\n")
                    else:
                        print("Paso " + str(inicial))
                        sig = puntos[inicial] #Lista de puntos a pintar
                        x_sig = sig[0]
                        y_sig = sig[1]
                        print("El siguiente pixel a pintar es el: (" + str(x_sig) + ", " + str(y_sig) + ")")
                        for pix in ptos:
                            x = pix[0]
                            y = pix[1]
                            plot(x, y)
                        #Linea a trazar en cada paso
                        glBegin(GL_LINES)
                        negro = rgb(0, 0, 0)
                        glColor(negro)
                        glVertex2fv([50 * ptos[0][0] + 25, 50 * ptos[0][1] + 25])
                        glVertex2fv([50 * ptos[len(ptos) - 1][0] + 25, 50 * ptos[len(ptos) - 1][1] + 25])
                        glEnd()
                        pygame.display.flip()
                    
                    
                if event.key == pygame.K_RIGHT:

                    inicial += 1
                    grilla(ancho, alto)
                    ptos = puntos[:inicial]
                    if inicial > len(puntos) - 1:
                        print("\n**Se acabaron los pasos del algoritmo. No hagas mas pasos a la derecha, SOLO INTENTA RETROCEDER.**\n")
                    else:
                        print("Paso " + str(inicial))
                        sig = puntos[inicial]
                        x_sig = sig[0]
                        y_sig = sig[1]
                        print("El siguiente pixel a pintar es el: (" + str(x_sig) + ", " + str(y_sig) + ")")

                    for pix in ptos:
                        x = pix[0]
                        y = pix[1]
                        plot(x, y)

                    glBegin(GL_LINES)
                    negro = rgb(0, 0, 0)
                    glColor(negro)
                    glVertex2fv([50 * ptos[0][0] + 25, 50 * ptos[0][1] + 25])
                    glVertex2fv([50 * ptos[len(ptos) - 1][0] + 25, 50 * ptos[len(ptos) - 1][1] + 25])
                    glEnd()
                    pygame.display.flip()
                    


        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        pygame.time.wait(10)